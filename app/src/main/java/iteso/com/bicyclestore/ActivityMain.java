package iteso.com.bicyclestore;

import android.app.Activity;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.RadioGroup;
import android.widget.ScrollView;
import android.widget.Toast;

public class ActivityMain extends AppCompatActivity {

    ImageButton imageButton;
    RadioGroup radioGroup;
    Button sizeSmall;
    Button sizeMedium;
    Button sizeLarge;
    Button sizeExtraLarge;
    Button addToCart;
    ScrollView scrollView;
    CoordinatorLayout coordinatorLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        imageButton = findViewById(R.id.activity_main_like_btn);
        radioGroup = findViewById(R.id.activity_main_color_radio_group);

        sizeSmall = findViewById(R.id.activity_main_size_button_small);
        sizeMedium = findViewById(R.id.activity_main_size_button_medium);
        sizeLarge = findViewById(R.id.activity_main_size_button_large);
        sizeExtraLarge = findViewById(R.id.activity_main_size_button_extra_large);
        addToCart = findViewById(R.id.activity_main_add_to_cart);
        scrollView = findViewById(R.id.activity_main_scroll_layout);
        coordinatorLayout = findViewById(R.id.activity_coordinator_container);

        radioGroup.clearCheck();

        imageButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast likeToast = Toast.makeText(ActivityMain.this, "+1 to Vintage Bicycle", Toast.LENGTH_LONG);
                likeToast.show();
            }
        });

        addToCart.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(addToCart.isSelected()){
                    Toast.makeText(ActivityMain.this, "Item already is in Cart", Toast.LENGTH_LONG).show();
                }else{
                    addToCart.setSelected(true);
                    addToCart.setText(R.string.activity_main_added_to_cart_text);
                    Snackbar.make(coordinatorLayout, "Added to cart", Snackbar.LENGTH_INDEFINITE).setAction("UNDO",
                            new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    addToCart.setSelected(false);
                                    addToCart.setText(R.string.activity_main_add_to_cart_text);
                                }
                            }).show();
                }
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        outState.putInt("COLOR", radioGroup.getCheckedRadioButtonId());
        outState.putBoolean("SMALL", sizeSmall.isSelected());
        outState.putBoolean("MEDIUM", sizeMedium.isSelected());
        outState.putBoolean("LARGE", sizeLarge.isSelected());
        outState.putBoolean("EXTRALARGE", sizeExtraLarge.isSelected());
        outState.putBoolean("ADDTOCART", addToCart.isSelected());
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        restoreState(savedInstanceState);
    }

    private void restoreState(Bundle savedInstanceState){
        radioGroup.check( savedInstanceState.getInt("COLOR"));
        setButtonState(sizeSmall, savedInstanceState.getBoolean("SMALL"));
        setButtonState(sizeMedium, savedInstanceState.getBoolean("MEDIUM"));
        setButtonState(sizeLarge, savedInstanceState.getBoolean("LARGE"));
        setButtonState(sizeExtraLarge, savedInstanceState.getBoolean("EXTRALARGE"));
        if(savedInstanceState.getBoolean("ADDTOCART") == true){
            addToCart.setSelected(true);
            addToCart.setText(R.string.activity_main_added_to_cart_text);
        }

    }

    public void setButtonState(Button btn, Boolean isSelected){
        if(isSelected == true){
            btn.setSelected(true);
            btn.setBackgroundResource(R.drawable.sizebtnpressed);
        }
    }

    //implement the onClick method here
    public void onSizeBtnClick(View v) {
        // Perform action on click
        switch (v.getId()) {
            case R.id.activity_main_size_button_small:
                sizeSmall.setBackgroundResource(R.drawable.sizebtnpressed);
                sizeSmall.setSelected(true);
                sizeMedium.setBackgroundResource(R.drawable.sizebtn);
                sizeMedium.setSelected(false);
                sizeLarge.setBackgroundResource(R.drawable.sizebtn);
                sizeLarge.setSelected(false);
                sizeExtraLarge.setBackgroundResource(R.drawable.sizebtn);
                sizeExtraLarge.setSelected(false);
                break;
            case R.id.activity_main_size_button_medium:
                sizeSmall.setBackgroundResource(R.drawable.sizebtn);
                sizeSmall.setSelected(false);
                sizeMedium.setBackgroundResource(R.drawable.sizebtnpressed);
                sizeMedium.setSelected(true);
                sizeLarge.setBackgroundResource(R.drawable.sizebtn);
                sizeLarge.setSelected(false);
                sizeExtraLarge.setBackgroundResource(R.drawable.sizebtn);
                sizeExtraLarge.setSelected(false);
                break;
            case R.id.activity_main_size_button_large:
                sizeSmall.setBackgroundResource(R.drawable.sizebtn);
                sizeSmall.setSelected(false);
                sizeMedium.setBackgroundResource(R.drawable.sizebtn);
                sizeMedium.setSelected(false);
                sizeLarge.setBackgroundResource(R.drawable.sizebtnpressed);
                sizeLarge.setSelected(true);
                sizeExtraLarge.setBackgroundResource(R.drawable.sizebtn);
                sizeExtraLarge.setSelected(false);
                break;
            case R.id.activity_main_size_button_extra_large:
                sizeSmall.setBackgroundResource(R.drawable.sizebtn);
                sizeSmall.setSelected(false);
                sizeMedium.setBackgroundResource(R.drawable.sizebtn);
                sizeMedium.setSelected(false);
                sizeLarge.setBackgroundResource(R.drawable.sizebtn);
                sizeLarge.setSelected(false);
                sizeExtraLarge.setBackgroundResource(R.drawable.sizebtnpressed);
                sizeExtraLarge.setSelected(true);
                break;
            default:
                break;
        }
    }
}
